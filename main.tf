provider "aws" {
  region = "eu-west-1"
}

# This will work with Terraform and RenovateBot's git regex will match it.
module "https" {
  source = "git::https://gitlab.com/schafferg/tf-module.git?ref=0.0.1"

  bucket_name_suffix = "https"
}

# This will work with Terraform and RenovateBot's git regex will match it.
module "ssh" {
  source = "git::git@gitlab.com:schafferg/tf-module.git?ref=0.0.1"

  bucket_name_suffix = "ssh"
}
